using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Timers;
using System.Text;

public class BallController : MonoBehaviour {

    private Rigidbody rb;
    public Menu menu;
    public int count;
    public int gameNbr;
    public Text gameNbrText;
    public Text countText;
    public Text timerText;
    public Text currAvgText;
    public Text HitOrMiss;
    public Text StatsText;
    public float currCountValue;
    private float startTime;
    private float pause = Time.time;
    private float t;
    private float avg;
    private float totaltime;
    private float roundTime;
    private float endTime;
    private int round;
    public ColorBlinker cb;
    public GameObject[] Goals;
    public GameObject target;
    public Material material;
    public string parentName;
    public Vector3 originalPos;
    public bool waiting;
    public bool hit;
    public bool timerOn;
    public bool playing = false;
    private ArrayList scores;
    private ArrayList counters;

    void Start()
    {
        gameNbr = 0;
        StatsText.text = "testing";
        Time.timeScale = 1f;
        timerOn = false;
        originalPos = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
        rb = GetComponent<Rigidbody>();
        playing = false;
        count = 0;
        scores = new ArrayList();
        counters = new ArrayList();
    }

    void FixedUpdate()
    {
        if (playing)
        {
            if (!timerOn)
                return;

            t = Time.time - startTime;
            string seconds = (t % 60).ToString("f2");
        } 
    }

    private IEnumerator ShowGoalText()
    {
        HitOrMiss.text = "GOAL!";
        int curr = 0;
        while (curr < 2)
        {
            yield return new WaitForSeconds(1.0f);
            curr++;
        }
        HitOrMiss.text = " ";
    }

    private void OnTriggerEnter(Collider other)
    {
        string hitObj = other.gameObject.transform.parent.gameObject.ToString();

         if (!other.gameObject.CompareTag("Pointer")){
            if (other.gameObject.CompareTag("shoe"))
            {
                endTime = Time.time; //tid d� man tr�ffar bollen
            }
            if (hitObj.Equals(parentName))
            {
                roundTime = Time.time - startTime;
                totaltime = totaltime + roundTime;
                StartCoroutine(ShowGoalText());
                count++;

            }
            if (round < 10)
            { 
                Reset();
            }
            else
            {
                countText.text = "Your Score: " + count.ToString();
                currAvgText.text = "Average time: " + (totaltime / count);
                menu.End();
            }
        } else {
        }
    }

    private IEnumerator SetNewGoal()
    {
       if (target != null)
       {
            cb.TurnOff();
        }
        Goals = GameObject.FindGameObjectsWithTag("Frame");
        int i = Random.Range(0, 7);
        target = Goals[i];
        parentName = target.gameObject.transform.parent.gameObject.ToString();
        int randomTime = Random.Range(1, 10);
        currCountValue = 0;
        while (currCountValue < randomTime)
        {
            yield return new WaitForSeconds(1.0f);
            currCountValue++;
        }
        target.SetActive(true);
        cb = target.GetComponent<ColorBlinker>();
        cb.TurnOn();
        startTime = Time.time;
        timerOn = true;
        HitOrMiss.text = "";
    }

    private void Reset()
    {
        timerOn = false;
        round++;
        Debug.Log("Round: " + round);
        gameObject.transform.position = originalPos;
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        StartCoroutine(SetNewGoal());
    }

    public void CalcAvgTime()
    {
        avg = (totaltime / count);
        scores.Add(avg);
        counters.Add(count);
    }

    public void StatsToString()
    {
        StringBuilder sb = new StringBuilder("");
        sb.AppendLine();
        for(int i=0; i<scores.Count; i++)
        {
            sb.Append((i+1)+ "         ");
            sb.Append(scores[i].ToString()+ "         ");
            sb.AppendLine(counters[i].ToString());
        }
        StatsText.text = sb.ToString();
    }

    public void StartNewSession()
    {
        Time.timeScale = 1f;
        gameNbr++;
        count = 0;
        round = 0;
        playing = true;
        totaltime = 0;
        Reset();  
    }

    public void EndSession()
    {
        CalcAvgTime();
        timerOn = false;
        playing = false;
        Time.timeScale = 1f;
        timerText.text = " ";

    }

    public void SetPause()
    {
        playing = false;
        Time.timeScale = 1f;
    }

    public void SetPlay()
    {
        playing = true;
        Time.timeScale = 1f;
    }

}