﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorBlinker : MonoBehaviour
{
    public float FadeDuration = 1f;
    public Color Color1 = Color.white;
    public Color Color2 = Color.red;
    public bool isOn = false;
    private Color startColor;
    private Color endColor;
    private float lastColorChangeTime;
    public float Timer;

    private Material material;

    void Awake()
    {
        material = GetComponent<Renderer>().material;
        startColor = Color1;
        endColor = Color2;
        isOn = false;
    }

    void Update()
    {
        if (isOn)
        {
            var ratio = (Time.time - lastColorChangeTime) / FadeDuration;
            ratio = Mathf.Clamp01(ratio);
            material.color = Color.Lerp(startColor, endColor, ratio);

            if (ratio == 1f)
            {
                lastColorChangeTime = Time.time;

                // Switch colors
                var temp = startColor;
                startColor = endColor;
                endColor = temp;
            }
        } else
        {
            Debug.Log("isOn == false");
        }
    }

    public void TurnOff()
    {
        Debug.Log("Turning blinkers OFF");
        material.color = Color1;
        isOn = false;
    }

    public void TurnOn()
    {
        Debug.Log("Turning blinkers ON");
        isOn = true;
    }

    public float GetTime()
    {
        return Timer;
    }
}