﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Valve.VR;

public class Menu : MonoBehaviour
{
    public BallController bc;
    public bool collision;
    public GameObject welcomeUI;
    public GameObject mainMenuUI;
    private GameObject collidingObject;
    public GameObject pauseMenu;
    public GameObject tutorial;
    public GameObject stats;
    public GameObject wall;
    public static bool gameIsPaused;
    public static bool hasActiveGame;
    Material material;

    private void Start()
    {
        Time.timeScale = 1f;
        welcomeUI.SetActive(true);
        mainMenuUI.SetActive(false);
        pauseMenu.SetActive(false);
        tutorial.SetActive(false);
        stats.SetActive(false);
        wall.SetActive(true);
        hasActiveGame = false;
    }

    void Update()
    {

        if (SteamVR_Input._default.inActions.GrabPinch.GetStateDown(SteamVR_Input_Sources.Any))
        {
            Debug.Log("Button press");
            Debug.Log("collision " + collision);
            if (collision)
            {
                Debug.Log("Clicked on " + collidingObject.tag);

                if (collidingObject.CompareTag("Start"))
                {
                    StartGame();
                }
                else if (collidingObject.CompareTag("Tutorial"))
                {
                    StartTutorial();
                }
                else if (collidingObject.CompareTag("Stats"))
                {
                    ShowStats();
                }
                else if (collidingObject.CompareTag("Quit"))
                {
                    QuitGame();
                }
                else if (collidingObject.CompareTag("BackMain"))
                {
                    Back();
                }
                else if (collidingObject.CompareTag("Resume"))
                {
                    Resume();
                }
                else if (collidingObject.CompareTag("EndSession"))
                {
                    End();
                }

                else
                {
                    Debug.Log("No object clicked");
                    if (hasActiveGame)
                    {
                        Debug.Log("Game is active");
                        if (!gameIsPaused)
                        {
                            Debug.Log("Game is playing, pausing on air click");
                            Pause();
                        }
                        else
                        {
                            Resume();
                        }
                    }
                }
            }
            else
            {
                if (hasActiveGame)
                {
                    if (!gameIsPaused)
                    {
                        Pause();
                    }
                    else
                    {
                        Resume();
                    }
                }
            }
            collision = false;
            collidingObject = null;
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        collidingObject = other.gameObject;
        collision = true;

    }

    public void StartGame()
    {
        hasActiveGame = true;
        gameIsPaused = false;
        wall.SetActive(false);
        mainMenuUI.SetActive(false);
        pauseMenu.SetActive(false);
        welcomeUI.SetActive(false);
        bc.StartNewSession();
    }

    public void StartTutorial()
    {
        tutorial.SetActive(true);
        mainMenuUI.SetActive(false);
        welcomeUI.SetActive(false);
    }

    public void ShowStats()
    {
        stats.SetActive(true);
        mainMenuUI.SetActive(false);
        welcomeUI.SetActive(false);
        bc.StatsToString();
    }

    public void QuitGame()
    {
        UnityEditor.EditorApplication.isPlaying = false;
    }

    public void Resume()
    {
        wall.SetActive(false);
        pauseMenu.SetActive(false);
        mainMenuUI.SetActive(false);
        tutorial.SetActive(false);
        stats.SetActive(false);
        welcomeUI.SetActive(false);
        gameIsPaused = false;
        bc.SetPlay();
    }

    public void Pause()
    {
        wall.SetActive(true);
        pauseMenu.SetActive(true);
        welcomeUI.SetActive(false);
        gameIsPaused = true;
        bc.SetPause();
    }

    public void End()
    {
        wall.SetActive(true);
        mainMenuUI.SetActive(true);
        pauseMenu.SetActive(false);
        welcomeUI.SetActive(false);
        hasActiveGame = false;
        gameIsPaused = false;
        bc.EndSession();
    }

    public void Back()
    {
        tutorial.SetActive(false);
        stats.SetActive(false);
        mainMenuUI.SetActive(true);
        welcomeUI.SetActive(false);
    }

}
