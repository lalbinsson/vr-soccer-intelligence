﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayCast : MonoBehaviour
{
    public float m_DefaultLength = 10;
    public Menu menu;
    public GameObject m_Dot;
    private LineRenderer m_LineRenderer = null;

    private void Awake()
    {
            m_LineRenderer = GetComponent<LineRenderer>();
    }

    private void Update()
    {
            float targetLength = m_DefaultLength;
            RaycastHit hit = CreateRayCast(targetLength);
            Vector3 endPosition = transform.position + (transform.forward * targetLength);
            m_Dot.transform.position = endPosition;
            if (hit.collider != null)
            {
                endPosition = hit.point;
            }
            m_Dot.transform.position = endPosition;
            m_LineRenderer.SetPosition(0, transform.position);
            m_LineRenderer.SetPosition(1, endPosition);
    }

    private RaycastHit CreateRayCast(float length)
    {
        RaycastHit hit;
        Ray ray = new Ray(transform.position, transform.forward);
        Physics.Raycast(ray, out hit, m_DefaultLength);
        return hit;
    }

}
