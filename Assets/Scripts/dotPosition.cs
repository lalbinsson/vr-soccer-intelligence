﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dotPosition : MonoBehaviour
{

    public float m_DefaultLength = 10;
    public GameObject dot;
    Vector3 pos;

    void Start()
    {
            pos = new Vector3(dot.transform.position.x, dot.transform.position.y, dot.transform.position.z);
    }

    void Update()
    {
            dot.transform.position = pos;
            this.transform.forward = dot.transform.forward;
    }
    

    public void OnTriggerEnter(Collider other)
    {
            pos = other.gameObject.transform.position;
    }
}
